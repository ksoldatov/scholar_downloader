import logging


class Logger:
    @staticmethod
    def get_logger():
        logging.basicConfig(format='%(asctime)s - %(filename)s - %(funcName)s - %(levelname)s | %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.INFO)
        root = logging.getLogger("stdout")
        root.setLevel(logging.DEBUG)
        return root
