import argparse
import ntpath

from scholar_downloader import Downloader

parser = argparse.ArgumentParser()
parser.add_argument("--query", "-q", help="query to search books", type=str)
parser.add_argument("--download-folder", "-d", help="folder to save results", type=str)
parser.add_argument("--count", "-c", help="how many results to save",
                    type=int)
argparser = parser.parse_args()


def run():
    downloader = Downloader(query=argparser.query, folder=argparser.download_folder, link_count=argparser.count)
    downloader.form_request()

    for link in downloader.document_links:
        filename = ntpath.basename(link)
        downloader.download_file(link, filename)


if __name__ == '__main__':
    run()
