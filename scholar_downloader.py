from selenium.webdriver.chrome.options import Options
import errno
import os
import time
from urllib.parse import urlencode, quote_plus

import requests
from selenium import webdriver

from logger import Logger


class Downloader:
    _log = None
    _input_query = None
    _request_url = 'https://scholar.google.ru/scholar'
    _download_folder = None
    _link_count = None
    _next_link = None

    document_links = list()

    def __init__(self, query, folder, link_count):
        self._log = Logger.get_logger()
        self.set_query(query)
        self.set_download_folder(folder)
        self.set_links_count(link_count)
        self._log.debug('Logger initialized successfully')

    def set_query(self, query):
        self._input_query = query
        self._log.debug("query set to {}".format(query))

    def set_download_folder(self, folder):
        self._download_folder = folder
        self._log.debug("download folder set to {}".format(folder))

    def set_links_count(self, link_count):
        self._link_count = link_count
        self._log.debug("link count set to {}".format(link_count))

    def form_request(self):
        if self._input_query:
            self._log.debug('forming input query ...')

            query_args = {
                'q': '{} filetype:pdf'.format(self._input_query),
            }
            query_args_urlencoded = urlencode(query_args, quote_via=quote_plus)
            self._request_url = '{}?{}'.format(self._request_url, query_args_urlencoded)
            self._log.debug('result url : {}'.format(self._request_url))

            self.collect_document_links()

        else:
            self._log.error("_input_query is None, shutting down")
            exit(1)

    def collect_document_links(self, timeout=300):
        chrome_options = Options()
        chrome_options.add_argument("--headless") # not used right now
        driver = webdriver.Chrome()
        time_started = time.time()

        driver.get(self._request_url)
        while time.time() - time_started < timeout:
            self._next_link = driver.find_element_by_link_text('Следующая')
            all_links = driver.find_elements_by_xpath("//a[@href]")
            for elem in all_links:
                link = elem.get_attribute("href")
                if os.path.splitext(link)[1] == '.pdf' and link not in self.document_links and len(
                        self.document_links) < self._link_count:
                    self._log.info("found link {}".format(link))
                    self.document_links.append(link)

            if len(self.document_links) < self._link_count:
                if self._next_link:
                    self._log.info('Downloading next page ... ')
                    self._next_link.click()
                else:
                    self._log.warn("only {} books found, but there is no next page.".format(
                        len(self.document_links)))
                    driver.close()
                    break
            else:
                self._log.info("{} required links found".format(len(self.document_links)))
                driver.close()
                break

    def download_file(self, url, filename):
        old_cwd = os.getcwd()

        try:
            os.makedirs(self._download_folder)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise Exception(e)

        os.chdir(self._download_folder)

        if os.path.isfile(filename):
            self._log.warn("file {} already exists, skip downloading {}".format(filename, url))
            os.chdir(old_cwd)
            return

        try:
            download_request = requests.get(url, allow_redirects=True)
            content_type = download_request.headers.get('content-type')
        except Exception as e:
            self._log.error("error happened during downloading {}\n{}".format(url, e))
            os.chdir(old_cwd)
            return

        if content_type == 'application/pdf':
            try:
                with open(filename, 'wb') as f:
                    self._log.debug('downloading {} to {}'.format(url, filename))
                    f.write(download_request.content)
            except Exception as e:
                self._log.error("Error happened: {}".format(e))
        else:
            self._log.warn('{} content-type is not application/pdf, skipping'.format(url))
        os.chdir(old_cwd)
